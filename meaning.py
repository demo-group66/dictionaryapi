import json
import argparse
import urllib.request

# get the lookup keyword
parser = argparse.ArgumentParser()
parser.add_argument("keyword", type=str)
args = parser.parse_args()
query = args.keyword

# api_key
apikey = open("apikey.txt", 'r').readline()

dict_url = "https://dictionaryapi.com/api/v3/references/collegiate/json/"+query+"?key=" + apikey
response = urllib.request.urlopen(dict_url)
resp_json = json.load(response)

for record in resp_json:
    definitions = record['shortdef']
    if record['meta']['id'] != query:
        print("\nid: "+record['meta']['id'])
    try:
        print('fl: '+record['fl'])
        for i, definition in enumerate(definitions, 1):
            print(str(i) + ". " + definition)
    except KeyError:
        pass
