# dictapi



## Intro

dictapi is a command interface to query merriam webster dictionary.

## Requirement

- You will need to register and get your api key at https://dictionaryapi.com
- Update apikey.txt with your api key after cloning the repo

## Clone the repo

```
git clone https://gitlab.com/demo-group66/dictionaryapi.git
```

## Usage
$ python3 meaning.py <lookup_word>
```
example: python3 meaning.py exercise
```
