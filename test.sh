#!/bin/bash

#validate the url
status_code=$(curl -w "%{http_code}\n" -s -o /dev/null "https://dictionaryapi.com/api/v3/references/collegiate/json/exercise?key=dummy")

if [[ "$status_code" -ne 200 ]] ; then
  echo "response code: $status_code"
  exit 1
else
  exit 0
fi
